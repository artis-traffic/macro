/**
 * @file artis-traffic/macro/core/Node.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <artis-traffic/macro/core/Node.hpp>

#include <iostream>

namespace artis::traffic::macro::core {

void Node::transition(const artis::traffic::core::Bag &bag,
                      const artis::traffic::core::Time & t) {
  std::for_each(bag.begin(), bag.end(),
                [this](const artis::traffic::core::ExternalEvent &event) {
                  if (event.port_index() >= inputs::IN_UP) {
                    LinkData d{};

                    event.data()(d);
                    _in_next_link_data[event.port_index() - inputs::IN_UP] = d;
                  } else if (event.port_index() >= inputs::IN_DOWN) {
                    LinkData d{};

                    event.data()(d);
                    _in_previous_link_data[event.port_index() - inputs::IN_DOWN] = d;
                  } else if (event.on_port(inputs::IN_OPEN)) {
                  } else if (event.on_port(inputs::IN_CLOSE)) {
                  }
                });
  // new state
  compute(t);
}

void Node::start(const artis::traffic::core::Time & t) {
  for (size_t i = 0; i < _in_proportions.size(); ++i) {
    _in_next_link_data = LinkData{0, 0, 0, _initial_next_supply[i]};
    _in_previous_link_data = LinkData{0, 0, _initial_previous_demand[i], 0};
  }
  compute(t);
}

artis::traffic::core::Bag Node::lambda(const artis::traffic::core::Time & /* t */) const {
  artis::traffic::core::Bag bag;

  for (size_t i = 0; i < _out_previous_link_data.size(); ++i) {
    bag.push_back(artis::traffic::core::ExternalEvent(outputs::OUT_UP + (int) i, _out_previous_link_data[i]));
  }
  for (size_t i = 0; i < _out_next_link_data.size(); ++i) {
    bag.push_back(artis::traffic::core::ExternalEvent(outputs::OUT_DOWN + (int) i, _out_next_link_data[i]));
  }
  return bag;
}

artis::common::event::Value Node::observe(const artis::traffic::core::Time & /* t */,
                                          unsigned int /* index */) const {
  return {};
}

void Node::compute(const artis::traffic::core::Time & /* t */)
{
  double previous_demand_sum = 0;

  for (size_t i = 0; i < _in_previous_link_data.size(); ++i) {
    previous_demand_sum += _in_previous_link_data[i].demand;
  }
  std::transform(std::begin(_out_proportions), std::end(_out_proportions), std::begin(_out_next_link_data),
                 [previous_demand_sum](const double &d) -> LinkData {
                   return LinkData{0, 0, previous_demand_sum, 0};
                 });

  double next_supply_sum = 0;

  for (size_t i = 0; i < _in_next_link_data.size(); ++i) {
    next_supply_sum += _out_proportions[i] * _in_next_link_data[i].supply;
  }
  std::transform(std::begin(_in_proportions), std::end(_in_proportions), std::begin(_out_previous_link_data),
                 [next_supply_sum](const double &d) -> LinkData {
                   return LinkData{0, 0, 0, next_supply_sum};
                 });
}

}