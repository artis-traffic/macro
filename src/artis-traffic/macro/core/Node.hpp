/**
 * @file artis-traffic/macro/core/Node.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MACRO_CORE_NODE_HPP
#define ARTIS_TRAFFIC_MACRO_CORE_NODE_HPP

#include <artis-star/kernel/dtss/Coordinator.hpp>
#include <artis-star/kernel/dtss/Dynamics.hpp>

#include <artis-traffic/core/Base.hpp>
#include <artis-traffic/macro/core/Link.hpp>

#include <valarray>

namespace artis::traffic::macro::core {

struct NodeParameters {
  std::valarray<double> in_proportions;
  std::valarray<double> out_proportions;
  std::valarray<double> initial_previous_demand;
  std::valarray<double> initial_next_supply;
};

class Node
  : public artis::dtss::Dynamics<artis::common::DoubleTime, Node, NodeParameters> {
public:
  struct inputs {
    enum values {
      IN_OPEN, IN_CLOSE, IN_DOWN, IN_UP = 100
    };
  };

  struct outputs {
    enum values {
      OUT_OPEN, OUT_CLOSE, OUT_DOWN, OUT_UP = 100
    };
  };

//  struct vars {
//    enum values {
//    };
//  };

  Node(const std::string &name,
       const artis::dtss::Context<artis::common::DoubleTime, Node, NodeParameters> &context)
    :
    artis::dtss::Dynamics<artis::common::DoubleTime, Node, NodeParameters>(name, context),
    _in_proportions(context.parameters().in_proportions),
    _out_proportions(context.parameters().out_proportions),
    _initial_previous_demand(context.parameters().initial_previous_demand),
    _initial_next_supply(context.parameters().initial_next_supply),
    _out_previous_link_data(_in_proportions.size()),
    _out_next_link_data(_out_proportions.size()),
    _in_previous_link_data(_in_proportions.size()),
    _in_next_link_data(_out_proportions.size()) {
    input_ports({{inputs::IN_OPEN,  "in_open"},
                 {inputs::IN_CLOSE, "in_close"}
                });
    output_ports({{outputs::OUT_OPEN,  "out_open"},
                  {outputs::OUT_CLOSE, "out_close"}
                 });
    for (unsigned int i = 0; i < _in_proportions.size(); ++i) {
      std::stringstream ss;

      ss << "in_down_" << (i + 1);
      input_port({inputs::IN_DOWN + i, ss.str()});
      ss << "out_up_" << (i + 1);
      output_port({outputs::OUT_UP + i, ss.str()});
    }
    for (unsigned int i = 0; i < _out_proportions.size(); ++i) {
      std::stringstream ss;

      ss << "in_up_" << (i + 1);
      input_port({inputs::IN_UP + i, ss.str()});
      ss << "out_down_" << (i + 1);
      output_port({outputs::OUT_DOWN + i, ss.str()});
    }
//    observables();
  }

  ~Node() override = default;

  void transition(const artis::traffic::core::Bag &bag,
                  const artis::traffic::core::Time &t) override;

  void start(const artis::traffic::core::Time & /* t */)
  override;

  artis::traffic::core::Bag lambda(const artis::traffic::core::Time & /* t */) const
  override;

  artis::common::event::Value observe(const artis::traffic::core::Time &t,
                                      unsigned int index) const override;

private:
  // private method
  void compute(const artis::traffic::core::Time &t);

  // types
  struct OpenClosePhase {
    enum values {
      OPENED, SEND_OPEN, SEND_CLOSE, CLOSED
    };

    static std::string to_string(const values &value) {
      switch (value) {
        case OPENED:
          return "OPENED";
        case SEND_OPEN:
          return "SEND_OPEN";
        case SEND_CLOSE:
          return "SEND_CLOSE";
        case CLOSED:
          return "CLOSED";
      }
      return "";
    }
  };

  struct Phase {
    enum values {
    };

    static std::string to_string(const values &value) {
      switch (value) {
      }
      return "";
    }
  };

  // parameters
  const std::valarray<double> _in_proportions; // %
  const std::valarray<double> _out_proportions; // %
  const std::valarray<double> _initial_previous_demand;
  const std::valarray<double> _initial_next_supply;

  // state
  std::valarray<LinkData> _out_previous_link_data;
  std::valarray<LinkData> _out_next_link_data;

  std::valarray<LinkData> _in_previous_link_data;
  std::valarray<LinkData> _in_next_link_data;
};

}

#endif