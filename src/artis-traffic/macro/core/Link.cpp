/**
 * @file artis-traffic/macro/core/Link.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <artis-traffic/macro/core/Link.hpp>

namespace artis::traffic::macro::core {

void Link::transition(const artis::traffic::core::Bag &bag,
                      const artis::traffic::core::Time &t) {
  std::for_each(bag.begin(), bag.end(),
                [this](const artis::traffic::core::ExternalEvent &event) {
                  if (event.on_port(inputs::IN_DOWN)) {
                    LinkData d{};

                    event.data()(d);
                    _previous_link_data = d;

//                    std::cout << "DOWN: " << d.demand << std::endl;

                  } else if (event.on_port(inputs::IN_UP)) {
                    LinkData d{};

                    event.data()(d);
                    _next_link_data = d;

//                    std::cout << "UP: " << d.supply << std::endl;

                  } else if (event.on_port(inputs::IN_OPEN)) {
//                    _output_flow = _last_output_flow;
                  } else if (event.on_port(inputs::IN_CLOSE)) {
//                    _output_flow = 0;
//                    _last_output_flow = _output_flow;
                  }
                });

  // new state

  std::transform(std::begin(_density), std::end(_density), std::begin(_supply),
                 [=](const double &d) -> double {
                   return std::min(_flow_max, compute_flow_congestion(d));
                 });
  std::transform(std::begin(_density), std::end(_density), std::begin(_demand),
                 [=](const double &d) -> double {
                   return std::min(_flow_max, compute_flow_free(d));
                 });

  std::valarray<double> upstream_flow(0., _cell_number);
  std::valarray<double> downstream_flow(0., _cell_number);

  for (size_t k = 0; k < _cell_number; ++k) {
    if (k > 0) {
      upstream_flow[k] = std::min(_supply[k], _demand[k - 1]);
    } else {
      upstream_flow[k] = std::min(_supply[k], _previous_link_data.demand);
    }
    if (k < _cell_number - 1) {
      downstream_flow[k] = std::min(_supply[k + 1], _demand[k]);
    } else {
      downstream_flow[k] = std::min(_next_link_data.supply, _demand[k]);
    }
  }

  for (size_t k = 0; k < _cell_number; ++k) {
    _density[k] += 1. / _delta_x * (upstream_flow[k] - downstream_flow[k]) * time_step();
    _flow[k] = compute_flow(_density[k]);
  }

//  std::cout << t << " " << get_name() << " " << _previous_link_data.flow << " " << _previous_link_data.demand << " ";
//  std::cout << _next_link_data.flow << " " << _next_link_data.supply << " ";
//  std::for_each(std::begin(_density), std::end(_density), [](const double &d) { std::cout << d << " "; });
//  std::for_each(std::begin(_flow), std::end(_flow), [](const double &f) { std::cout << f << " "; });
//  std::cout << std::endl;
}

void Link::start(const artis::traffic::core::Time & /* t */) {
  _flow = _initial_output_flow;
  _density = _initial_output_flow / _free_flow_speed;
}

artis::traffic::core::Bag Link::lambda(const artis::traffic::core::Time & /* t */) const {
  artis::traffic::core::Bag bag;

  if (std::abs(_density[0] - _density_max) < 1e-6) {
    bag.push_back(artis::traffic::core::ExternalEvent(outputs::OUT_CLOSE));
  }
  bag.push_back(artis::traffic::core::ExternalEvent(outputs::OUT_DOWN,
                                                    LinkData{_flow[_cell_number - 1], _density[_cell_number - 1],
                                                             _demand[_cell_number - 1], _supply[_cell_number - 1]}));
  bag.push_back(artis::traffic::core::ExternalEvent(outputs::OUT_UP,
                                                    LinkData{_flow[0], _density[0], _demand[0], _supply[0]}));
  return bag;
}

artis::common::event::Value Link::observe(const artis::traffic::core::Time & /* t */,
                                          unsigned int /* index */) const {
  return {};
}

}