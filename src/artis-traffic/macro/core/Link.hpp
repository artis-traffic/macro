/**
 * @file artis-traffic/macro/core/Link.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MACRO_CORE_LINK_HPP
#define ARTIS_TRAFFIC_MACRO_CORE_LINK_HPP

#include <artis-star/kernel/dtss/Coordinator.hpp>
#include <artis-star/kernel/dtss/Dynamics.hpp>

#include <artis-traffic/core/Base.hpp>

#include <valarray>

namespace artis::traffic::macro::core {

struct LinkData {
  double flow;
  double density;
  double demand;
  double supply;

  LinkData(double flow = 0.0, double density = 0.0, double demand = 0.0, double supply = 0.0) {}

  bool operator==(const LinkData &other) const {
    return flow == other.flow and density == other.density and demand == other.demand and supply == other.supply;
  }

  std::string to_string() const {
    return "LinkData<" + std::to_string(flow) + " ; " + std::to_string(density) + " ; " + std::to_string(demand) +
           " ; " + std::to_string(supply) + ">";
  }

};

struct LinkParameters {
  double length;
  double delta_x;
  double free_flow_speed;
  double gap_time;
  double vehicle_length;
  unsigned int lane_number;
  double initial_output_flow;
};

class Link
  : public artis::dtss::Dynamics<artis::common::DoubleTime, Link, LinkParameters> {
public:
  struct inputs {
    enum values {
      IN_DOWN, IN_UP, IN_OPEN, IN_CLOSE
    };
  };

  struct outputs {
    enum values {
      OUT_DOWN, OUT_UP, OUT_OPEN, OUT_CLOSE
    };
  };

//  struct vars {
//    enum values {
//    };
//  };

  Link(const std::string &name,
       const artis::dtss::Context<artis::common::DoubleTime, Link, LinkParameters> &context)
    :
    artis::dtss::Dynamics<artis::common::DoubleTime, Link, LinkParameters>(name, context),
    _length(context.parameters().length),
    _delta_x(context.parameters().delta_x),
    _free_flow_speed(context.parameters().free_flow_speed),
    _gap_time(context.parameters().gap_time),
    _vehicle_length(context.parameters().vehicle_length),
    _lane_number(context.parameters().lane_number),
    _initial_output_flow(context.parameters().initial_output_flow),
    _cell_number(std::floor(_length / _delta_x)),
    _wave_speed(-_vehicle_length / _gap_time),
    _flow_max(_free_flow_speed / (_free_flow_speed * _gap_time + _vehicle_length)),
    _density_max(1. / _vehicle_length),
    _critical_density(_flow_max / _free_flow_speed),
    _flow(0., _cell_number),
    _density(0., _cell_number),
    _demand(0., _cell_number),
    _supply(0., _cell_number),
    _previous_link_data{},
    _next_link_data{} {
    input_ports({{inputs::IN_DOWN,  "in_down"},
                 {inputs::IN_UP,    "in_up"},
                 {inputs::IN_OPEN,  "in_open"},
                 {inputs::IN_CLOSE, "in_close"}
                });
    output_ports({
                   {outputs::OUT_DOWN,  "out_down"},
                   {outputs::OUT_UP,    "out_up"},
                   {outputs::OUT_OPEN,  "out_open"},
                   {outputs::OUT_CLOSE, "out_close"}
                 });
//    observables();
  }

  ~Link() override = default;

  void transition(const artis::traffic::core::Bag &bag,
                  const artis::traffic::core::Time &t) override;

  void start(const artis::traffic::core::Time & /* t */) override;

  artis::traffic::core::Bag lambda(const artis::traffic::core::Time & /* t */) const override;

  artis::common::event::Value observe(const artis::traffic::core::Time &t,
                                      unsigned int index) const override;

private:
  // private methods
  double compute_flow(double density) const {
    return density <= _critical_density ? compute_flow_free(density) : compute_flow_congestion(density);
  }

  double compute_flow_free(double density) const {
    return _free_flow_speed * density;
  }

  double compute_flow_congestion(double density) const {
    return _flow_max * (1. - _wave_speed / _free_flow_speed) + _wave_speed * density;
  }

  // types
  struct OpenClosePhase {
    enum values {
      OPENED, SEND_OPEN, SEND_CLOSE, CLOSED
    };

    static std::string to_string(const values &value) {
      switch (value) {
        case OPENED:
          return "OPENED";
        case SEND_OPEN:
          return "SEND_OPEN";
        case SEND_CLOSE:
          return "SEND_CLOSE";
        case CLOSED:
          return "CLOSED";
      }
      return "";
    }
  };

  struct Phase {
    enum values {
    };

    static std::string to_string(const values &value) {
      switch (value) {
      }
      return "";
    }
  };

  // parameters
  const double _length; // m
  const double _delta_x; // m
  const double _free_flow_speed; // m/s
  const double _gap_time; // s
  const double _vehicle_length; // m
  const unsigned int _lane_number;
  const double _initial_output_flow;

  // constants
  const std::size_t _cell_number;
  const double _wave_speed; // w (m/s)
  const double _flow_max; // Q_max (veh/s)
  const double _density_max; // rho_max (veh/m)
  const double _critical_density; // rho_max (veh/m)

  // state
  std::valarray<double> _flow; // Q
  std::valarray<double> _density; // rho
  std::valarray<double> _demand;
  std::valarray<double> _supply;
  LinkData _previous_link_data;
  LinkData _next_link_data;
//  double _last_next_flow;
};

}

#endif