/**
 * @file artis-traffic/macro/utils/Generator.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MACRO_UTILS_GENERATOR_HPP
#define ARTIS_TRAFFIC_MACRO_UTILS_GENERATOR_HPP

#include <artis-star/kernel/dtss/Dynamics.hpp>
#include <artis-traffic/core/Base.hpp>
#include <artis-traffic/macro/core/Link.hpp>

#include <random>

namespace artis::traffic::macro::utils {

template<typename FlowFunction>
struct GeneratorParameters {
  double min;
  double max;
  double mean;
  double stddev;
  unsigned long seed;
  FlowFunction flow_function;
};

template<typename FlowFunction>
class Generator
  : public artis::dtss::Dynamics<artis::common::DoubleTime, Generator<FlowFunction>, GeneratorParameters<FlowFunction>> {
public:
  struct inputs {
    enum values {
      IN_OPEN, IN_CLOSE
    };
  };

  struct outputs {
    enum values {
      OUT
    };
  };

  struct vars {
    enum values {
    };
  };

  Generator(const std::string &name,
            const artis::dtss::Context<artis::common::DoubleTime, Generator<FlowFunction>, GeneratorParameters<FlowFunction>> &context)
    :
    artis::dtss::Dynamics<artis::common::DoubleTime, Generator<FlowFunction>, GeneratorParameters<FlowFunction>>(name, context),
    _min(context.parameters().min),
    _max(context.parameters().max),
    _sigma_distribution(context.parameters().mean, context.parameters().stddev),
    _flow_function(context.parameters().flow_function) {
    _generator.seed(context.parameters().seed);
    this->input_ports({{inputs::IN_OPEN,  "in_open"},
                       {inputs::IN_CLOSE, "in_close"}});
    this->output_ports({{outputs::OUT, "out"}});
//    observables();
  }

  ~Generator() override = default;

  void start(const artis::traffic::core::Time &t) override {
    _last_time = t;
    _sigma = _sigma_distribution(_generator);
    _sigma = _sigma <= _min ? _min : _sigma;
    _sigma = _sigma >= _max ? _max : _sigma;
  }

  void transition(const artis::traffic::core::Bag &bag, const artis::traffic::core::Time &t) override {
    std::for_each(bag.begin(), bag.end(),
                  [this](const artis::traffic::core::ExternalEvent &event) {
                    if (event.on_port(inputs::IN_OPEN)) {
                      _sigma = _sigma_distribution(_generator);
                      _sigma = _sigma <= _min ? _min : _sigma;
                      _sigma = _sigma >= _max ? _max : _sigma;
                    } else if (event.on_port(inputs::IN_CLOSE)) {
                      _sigma = artis::common::DoubleTime::infinity;
                    }
                  });
    if (t > _last_time + _sigma) {
      _last_time = t;
      _sigma = _sigma_distribution(_generator);
      _sigma = _sigma <= _min ? _min : _sigma;
      _sigma = _sigma >= _max ? _max : _sigma;
    }
  }

  artis::traffic::core::Bag lambda(const artis::traffic::core::Time &t) const override {
    artis::traffic::core::Bag bag;

    if (t == 0 or t > _last_time + _sigma) {
      bag.push_back(
        artis::traffic::core::ExternalEvent(outputs::OUT, _flow_function(t)));
    }
    return bag;
  }

  artis::common::event::Value observe(const artis::traffic::core::Time & /* t */,
                                      unsigned int /* index */) const override {
    return {};
  }

private:
  // parameters
  double _min;
  double _max;

  // state
  artis::traffic::core::Time _supposed_time_to_send;
  artis::traffic::core::Time _last_time;
  artis::traffic::core::Time _sigma;
  std::default_random_engine _generator;
  std::normal_distribution<double> _sigma_distribution;
  FlowFunction _flow_function;
};

}

#endif