/**
 * @file artis-traffic/macro/utils/End.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MACRO_UTILS_END_HPP
#define ARTIS_TRAFFIC_MACRO_UTILS_END_HPP

#include <artis-star/kernel/dtss/Dynamics.hpp>
#include <artis-traffic/core/Base.hpp>
#include <artis-traffic/macro/core/Link.hpp>

#include <random>

namespace artis::traffic::macro::utils {

struct EndParameters {
  double flow_max;
};

class End
  : public artis::dtss::Dynamics<artis::common::DoubleTime, End, EndParameters> {
public:
  struct inputs {
    enum values {
      IN_DOWN, IN_OPEN, IN_CLOSE
    };
  };

  struct outputs {
    enum values {
      OUT_UP
    };
  };

  struct vars {
    enum values {
    };
  };

  End(const std::string &name,
      const artis::dtss::Context<artis::common::DoubleTime, End, EndParameters> &context)
    :
    artis::dtss::Dynamics<artis::common::DoubleTime, End, EndParameters>(name, context),
    _flow_max(context.parameters().flow_max) {
    input_ports({{inputs::IN_DOWN,  "in_down"},
                 {inputs::IN_OPEN,  "in_open"},
                 {inputs::IN_CLOSE, "in_close"}});
    output_ports({{outputs::OUT_UP, "out_up"}});
//    observables();
  }

  ~End() override = default;

  void start(const artis::traffic::core::Time & /* t */) override {
  }

  void transition(const artis::traffic::core::Bag & /* bag */, const artis::traffic::core::Time & /* t */) override {
  }

  artis::traffic::core::Bag lambda(const artis::traffic::core::Time & /* t */) const override {
    artis::traffic::core::Bag bag;

    bag.push_back(artis::traffic::core::ExternalEvent(outputs::OUT_UP, core::LinkData{_flow_max, 0, _flow_max, _flow_max}));
    return bag;
  }

  artis::common::event::Value observe(const artis::traffic::core::Time & /* t */,
                                      unsigned int /* index */) const override {
    return {};
  }

private:
  // parameters
  double _flow_max;
};

}

#endif